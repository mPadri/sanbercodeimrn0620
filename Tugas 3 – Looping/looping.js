console.log("Tugas 3 - Looping");
console.log("No 1 Looping While");

console.log(" ");
console.log("Looping Pertama");

var i = 1;
var tampil1 = "- I Love Coding";
while(i <= 20){
     if(i % 2 == 0 ){
          console.log(i + " " + tampil1);
     }
     i++;
}

console.log(" ");
console.log("Looping Kedua");

var j = 20;
var tampil2 = "- I will become a mobile developer";
while(j >= 1){
     if(j % 2 == 0 ){
          console.log(j + " " + tampil2);
     }
     j--;
}

console.log(" ");
console.log("No 2 Looping Menggunakan For");
var kata = " ";
for (let i = 1; i <= 20; i++) {
     if(i % 2 == 1 && i % 3 != 0){
           kata = "Santai";
          console.log(i + " " + kata);
     }

     if(i % 2 == 0){
           kata = "Berkualitas";
          console.log(i + " " + kata);
     }

     if(i % 3 == 0 && i % 2 == 1){
           kata = "I Love Coding";
          console.log(i + " " + kata);
     }

}

console.log(" ");
console.log("No 3 Membuat Persegi Panjang");

var tampil = "";
var k = 1;
while (k <= 4) {
     var l = 1;
     while (l <= 8) {

          tampil = tampil + "#";

          l++;
     }

     tampil = tampil + "\n";
     k++;
}

console.log(tampil);

console.log("No 4 Membuat Tangga");

var tampilkan = "";
var m = 1;

while(m <= 7){
     var n = 1;
     while(n <= m){
          tampilkan = tampilkan + "#";
          n++;
     }

     tampilkan = tampilkan + "\n";
     m++;
}

console.log(tampilkan);

console.log(" ");
console.log("No 5 Membuat Papan Catur");

var cetak = "";
var x = 1;
while (x <= 8) {
     if (x % 2 == 1) {

          var z = 1;
          while (z <= 8) {
               if (z % 2 == 0) {
                    cetak = cetak + "#";
               } else {
                    cetak = cetak + " ";
               }

               z++;
          }

     }else{

          var z = 1;
          while (z <= 8) {
               if (z % 2 == 1) {
                    cetak = cetak + "#";
               } else {
                    cetak = cetak + " ";
               }

               z++;
          }
     }

     cetak = cetak + "\n";
     x++;
}

console.log(cetak);


