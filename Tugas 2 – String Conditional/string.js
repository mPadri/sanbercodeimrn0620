console.log('Tugas String')
console.log('Soal No. 1 (Membuat kalimat)')
console.log(' ')

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var kalimat = word.concat(" ",second," ",third," ",fourth," ",fifth," ",sixth," ",seventh);

console.log(kalimat);

console.log(' ')
console.log('Soal No.2 Mengurai kalimat (Akses karakter dalam string)')
console.log(' ')

var sentence = "I am going to be React Native Developer";

var word3 = sentence[5] + sentence[6];
var word4 = sentence[11] + sentence[12];
var word5 = sentence[14] + sentence[15];
var word6 = sentence[16] + sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var word7 = sentence.charAt(22) + sentence.charAt(23) + sentence.charAt(24) + sentence.charAt(25) + sentence.charAt(26) + sentence.charAt(27) + sentence.charAt(28);
var word8 = sentence.charAt(30) + sentence.charAt(31) + sentence.charAt(32) + sentence.charAt(33) + sentence.charAt(34) + sentence.charAt(35) + sentence.charAt(36) + sentence.charAt(37) + sentence.charAt(38);

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = word3;
var fourthWord = word4;
var fifthWord = word5;
var sixthWord = word6;
var seventhWord = word7;
var eighthWord = word8;

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

console.log(' ')
console.log('Soal No. 3 Mengurai Kalimat (Substring)')
console.log(' ')

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4,14);
var thirdWord2 = sentence2.substring(15,17); 
var fourthWord2 = sentence2.substring(18,20); 
var fifthWord2 = sentence2.substr(21); 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

console.log(' ')
console.log('Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String')
console.log(' ')

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4,14);
var thirdWord3 = sentence3.substring(15,17); 
var fourthWord3 = sentence3.substring(18,20); 
var fifthWord3 = sentence3.substr(21); 

var firstWordLength = exampleFirstWord3.length;  
// lanjutkan buat variable lagi di bawah ini 
var secondWordLength = secondWord3.length;  
var thirdWordLength = thirdWord3.length;  
var fourthWordLength = fourthWord3.length;  
var fifthWordLength = fifthWord3.length;  

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 