console.log("Tugas 4 - Function");

console.log("No. 1");
function teriak(){
    var sapa = "Halo Sanbers!";
    return sapa;
}

console.log(teriak());

console.log(" ");
console.log("No. 2");

var kalikan = function (num1, num2){
    var hasil = num1 * num2;
    return hasil;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);

console.log(hasilKali);

console.log(" ");
console.log("No. 3");

function introduce(name, age, address, hobby){
    let kalimat = `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address} dan saya punya hobby yaitu ${hobby}`;
    return kalimat;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
