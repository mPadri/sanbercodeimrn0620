console.log('Tugas 7 – Class');
console.log('1. Animal Class');
console.log();
console.log('Release 0');

class Animal {
  constructor(nameAnimal) {
    this.legs = 4;
    this.cold_blooded = false;
    this._name = nameAnimal;
  }

  get name() {
    return this._name;
  }
}

var sheep = new Animal('shaun');

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

console.log('Release 1');

class Ape extends Animal {
  constructor(nameAnimal) {
    super(nameAnimal);
    this.legs = 2;
  }

  yell() {
    console.log('Auooo');
  }
}

class Frog extends Animal {
  constructor(nameAnimal) {
    super(nameAnimal);
  }

  jump() {
    console.log('hop hop');
  }
}

var sungokong = new Ape('kera sakti');
sungokong.yell(); // "Auooo"

var kodok = new Frog('buduk');
kodok.jump(); // "hop hop"

console.log('2. Function to Class');

class Clock {
  constructor({ template }) {
    this.timer = this.timer;
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) {
      return (hours = '0' + hours);
    }

    var mins = date.getMinutes();
    if (mins < 10) {
      return (mins = '0' + mins);
    }

    var secs = date.getSeconds();
    if (secs < 10) {
      return (secs = '0' + secs);
    }

    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => {
      return this.render();
    }, 1000);
  }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();
