var readBooksPromise = require('./promise.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

let time = 10000;

function loop(x) {
  if (x < books.length) {
    readBooksPromise(time, books[x])
      .then(() => {
        time = time - books[x].timeSpent;
        loop(x + 1);
      })
      .catch((err) => {
        return err;
      });
  }
}

loop(0);
