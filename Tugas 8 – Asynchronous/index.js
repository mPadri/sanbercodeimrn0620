var readBooks = require('./callback');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

let time = 10000;

function loop(x) {
  if (x < books.length) {
    readBooks(time, books[x], function (check) {
      if (check) {
        time = time - books[x].timeSpent;
        loop(x + 1);
      }
    });
  }
}

loop(0);
