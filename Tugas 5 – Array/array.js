console.log("Tugas 5 – Array")

console.log("Soal No. 1 (Range) ")

function range(startNum, finishNum) {
    let array = [];

    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i--) {
            array.push(i);
        }
    }

    if (startNum == null || finishNum == null) {
        return -1;
    }

    for (let i = startNum; i <= finishNum; i++) {
        array.push(i);

    }

    return array;


}

console.log(range(1, 10)); 
console.log(range(1));
console.log(range(11,18)); 
console.log(range(54, 50));
console.log(range()); 

console.log("Soal No. 2 (Range with Step) ")

function rangeWithStep(startNum, finishNum, step) {
    let array = [];

    if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i);
        }
    }
    for (let i = startNum; i <= finishNum; i += step) {
        array.push(i);
    }

    return array;

}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("Soal No. 3 (Sum of Range) ")

function sum(startNum, finishNum, step=1) {
    let array = [];
    
    if (startNum == null && finishNum == null) {
        return 0;
    }

    if (startNum == null || finishNum == null) {
        return 1;
    }


    if (startNum > finishNum) {
        
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i);
        }
    }

    for (let i = startNum; i <= finishNum; i += step) {
        array.push(i);
    }

    let total = array.reduce((x, y) => x + y);
    return total;
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log("Soal No. 4 (Array Multidimensi) ")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

const dataHandling =  function (input){
    let data = ""
    for(let i = 0; i<input.length; i++){
       
        data = ` Nomer ID: ${input[i][0]} \n Nama: ${input[i][1]} \n TTL: ${input[i][2]} ${input[i][3]} \n Hobi: ${input[i][4]}`
     
        console.log(data)
        console.log(" ")
       
    }
    return ""
}

console.log(dataHandling(input))

console.log("Soal No. 5 (Balik Kata) ")

function balikKata(kata){
    let data = ""
    for(let i = kata.length; i>=0; i--){
        data += kata.charAt(i)
    }

    return data
}


console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("Soal No. 6 (Metode Array) ")

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input2){


   input2.splice(1,1,'Roman Alamsyah Elsharawy')
   input2.splice(2,1,'Provinsi Bandar Lampung')
   input2.splice(4,0,'Pria')
   input2.splice(5,1,'SMA Internasional Metro')

   console.log(input2)
   let tgl = input2[0,3]
   let tglSplit = tgl.split('/');

   let ttl = tglSplit.join('-')
   tglSplit.sort(function(a,b){return b-a})

   let date = ttl.substr(3,2)
   let nama = input2.splice(1,1)
   let namaJoin = nama.join().slice(0,15)

   

   switch (date) {
        case "01":
            console.log("Januari")
            break;
        case "02":
            console.log("Februari")
            break;
        case "03":
            console.log("Maret")
            break;
        case "04":
            console.log("April")
            break;
        case "05":
            console.log("Mei")
            break;
        case "06":
            console.log("Juni")
            break;
        case "07":
            console.log("Juli")
            break;
        case "08":
            console.log("Agustus")
            break;
        case "09":
            console.log("September")
            break;
        case "10":
            console.log("Oktober")
            break;
        case "11":
            console.log("November")
            break;
        case "12":
            console.log("Desember")
            break;
    
        default:
            console.log("tidak ada")
            break;
    }

    console.log(tglSplit)
    console.log(ttl)
    console.log(namaJoin)

    return " ";
}

console.log(dataHandling2(input2))

