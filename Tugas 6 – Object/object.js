console.log('Tugas 6 – Object Literal');
console.log('Soal No. 1 (Array to Object)');

function arrayToObject(arr) {
  var obj = {};
  var now = new Date();
  var thisYear = now.getFullYear();

  if (!arr[0]) {
    console.log('""');
  } else {
    for (let i = 0; i < arr.length; i++) {
      obj.firstName = arr[i][0];
      obj.lastName = arr[i][1];
      obj.gender = arr[i][2];

      if (thisYear < arr[i][3] || arr[i][3] == null) {
        obj.age = 'Invalid birth year';
      } else {
        let umur = thisYear - arr[i][3];
        obj.age = umur;
      }
      console.log(`${obj.firstName} ${obj.lastName}:`, obj);
    }
  }
}

var people = [
  ['Bruce', 'Banner', 'male', 1975],
  ['Natasha', 'Romanoff', 'female'],
];
arrayToObject(people);

var people2 = [
  ['Tony', 'Stark', 'male', 1980],
  ['Pepper', 'Pots', 'female', 2023],
];
arrayToObject(people2);
arrayToObject([]);

console.log('');
console.log('Soal No. 2 (Shopping Time)');

function shoppingTime(memberId, money) {
  var obj = {
    memberId: memberId,
    money: money,
    listPurchased: [],
    changeMoney: money,
  };
  var product = [
    {
      namaProduct: 'Sepatu Stacattu',
      harga: 1500000,
    },
    {
      namaProduct: 'Baju Zoro',
      harga: 500000,
    },
    {
      namaProduct: 'Baju H&N',
      harga: 250000,
    },
    {
      namaProduct: 'Sweater Uniklooh',
      harga: 175000,
    },
    {
      namaProduct: 'Casing Handphone',
      harga: 50000,
    },
  ];

  if (memberId == null || memberId == '') {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  } else {
    let data = '';
    product.map((item) => {
      if (obj.money < item.harga || obj.money < 50000) {
        data = 'Mohon maaf, uang tidak cukup';
      }
      if (obj.changeMoney >= item.harga) {
        obj.listPurchased.push(item.namaProduct);
        obj.changeMoney = obj.changeMoney - item.harga;
        data = obj;
      }
    });
    return data;
  }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log('');
console.log('Soal No. 3 (Naik Angkot)');

function naikAngkot(arrPenumpang) {
  let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  let obj = {};

  if (!arrPenumpang.length) {
    return [];
  } else {
    for (let i = 0; i < arrPenumpang.length; i++) {
      obj.penumpang = arrPenumpang[i][0];
      obj.naikDari = arrPenumpang[i][1];
      obj.tujuan = arrPenumpang[i][2];

      let harga =
        rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]);
      obj.bayar = harga * 2000;
      console.log(obj);
    }
    return '';
  }
}

console.log(
  naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B'],
  ])
);

console.log(naikAngkot([]));
